from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.views import generic
from .models import Movie


class HomeView(generic.ListView):
    template_name = 'movie/home.html'
    context_object_name = 'all_movies'
    
    def get_queryset(self):
        return Movie.objects.order_by('title')


class DetailView(generic.DetailView):
    model = Movie
    template_name = 'movie/detail.html'
    
    
class MovieCreate(CreateView):
    model = Movie
    fields = ['title', 'genre', 'logo']
 
 
class MovieUpdate(UpdateView):
    model = Movie
    fields = ['title', 'genre']
    
    
class MovieDelete(DeleteView):
    model = Movie
    success_url = reverse_lazy('movie:home')
