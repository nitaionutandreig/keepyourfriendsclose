from django.contrib import admin
from movie.models import Movie, Actor


class MovieAdmin(admin.ModelAdmin):
    list_display = ['title', 'genre']
    

class ActorAdmin(admin.ModelAdmin):
    list_display = ['movie', 'name']
    

admin.site.register(Movie, MovieAdmin)
admin.site.register(Actor, ActorAdmin)
