from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = "movie"

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'movie/add/$', views.MovieCreate.as_view(), name='movie-add'),
    url(r'movie/(?P<pk>[0-9]+)/$', views.MovieUpdate.as_view(), name='movie-update'),
    url(r'movie/(?P<pk>[0-9]+)/delete/$', views.MovieDelete.as_view(), name='movie-delete'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)