from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

app_name = 'keepyourfriendsclose'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    
    url(r'^register/$', views.UserFormView.as_view(), name='register'),
    
    url(r'^login/$', views.UserFormView.as_view(), name='login'),
    
    url(r'^auth/$', views.UserFormView.as_view(), name='auth'),
    
    url(r'^logout/$', views.UserFormView.as_view(), name='logout'),
    
    url(r'^loggedin/$', views.UserFormView.as_view(), name='loggedin'),
    
    url(r'^invalid/$', views.UserFormView.as_view(), name='invalid'),
    
    url(r'^$', views.HomeView.as_view(), name='home'),
    
    url(r'^movie/', include('movie.urls')),
    
    url(r'^song/', include('song.urls')),
] 

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)