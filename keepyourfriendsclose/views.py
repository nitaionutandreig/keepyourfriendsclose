from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic import View
from .forms import UserForm

class HomeView(generic.ListView):
    template_name = 'keepyourfriendsclose/home.html'
    
    def get_queryset(self):
        pass
        
class UserFormView(View):
    form_class = UserForm
    template_name = 'keepyourfriendsclose/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})
        
    def post(self, request):
        form = self.form_class(request.POST)
        
        if form.is_valid():
            user = form.save(commit=False)
            
            # cleaned (normalied data)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()

            # returns User objects if credentials are correct
            user = authenticate(username=username, password=password)
            
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home')
                    
        return render(request, self.template_name, {'form': form})