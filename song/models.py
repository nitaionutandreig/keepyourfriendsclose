from django.db import models
from django.core.urlresolvers import reverse

        
class Song(models.Model):
    title = models.CharField(max_length=50)
    genre = models.CharField(max_length=50)
    logo = models.FileField()
    
    def __str__(self):
        return self.title + ' ' + self.genre
        
    def get_absolute_url(self):
        return reverse('song:detail', kwargs={'pk': self.pk})
    
    def __str__(self):
        return self.title + ' ' + self.genre
        
        
class Artist(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    picture = models.FileField()
    is_favorite = models.BooleanField(default=False)
    def __str__(self):
        return self.name