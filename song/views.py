from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect
from django.views import generic
from .models import Song

class HomeView(generic.ListView):
    template_name = 'song/home.html'
    context_object_name = 'all_songs'
    
    def get_queryset(self):
        return Song.objects.order_by('title')

class DetailView(generic.DetailView):
    model = Song
    template_name = 'song/detail.html'
    

class SongCreate(CreateView):
    model = Song
    fields = ['title', 'genre', 'logo']
 
 
class SongUpdate(UpdateView):
    model = Song
    fields = ['title', 'genre', 'logo']
    
    
class SongDelete(DeleteView):
    model = Song
    success_url = reverse_lazy('song:home')