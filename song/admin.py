from django.contrib import admin
from song.models import Song, Artist


class SongAdmin(admin.ModelAdmin):
    list_display = ['title', 'genre']
    

class ArtistAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_favorite', 'song_id']
    

admin.site.register(Song, SongAdmin)
admin.site.register(Artist, ArtistAdmin)
